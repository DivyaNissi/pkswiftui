//
//  ContentView.swift
//  PKSwiftUI
//
//  Created by Divya Kothagattu on 23/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import SwiftUI
import MapKit

struct ContentView: View {
    @State var Citys =
         [ City(name: "Bhubaneshwar", image: "Bhubaneshwar", population: "8.38 lakhs", coordinate: CLLocationCoordinate2D(latitude: 20.2961, longitude: 85.8245)),
                   City(name: "Pune", image: "Pune", population: "31.2 lakhs", coordinate: CLLocationCoordinate2D(latitude: 18.5204, longitude: 73.8567)),
                   City(name: "Jaipur", image: "Jaipur", population: "30.7 lakhs", coordinate: CLLocationCoordinate2D(latitude: 26.9124, longitude: 75.7873)),
                   City(name: "Surat", image: "Surat", population: "44.6 lakhs", coordinate: CLLocationCoordinate2D(latitude: 21.1702, longitude: 72.8311)),
                   City(name: "Kochi", image: "Kochi", population: "21.2 lakhs", coordinate: CLLocationCoordinate2D(latitude: 9.9312, longitude: 76.2673)),
                   City(name: "Ahmedabad", image: "Ahmedabad", population: "55.7 lakhs", coordinate: CLLocationCoordinate2D(latitude: 23.0225, longitude: 72.5714)),
                   City(name: "Jabalpur", image: "Jabalpur", population: "12.7 lakhs", coordinate: CLLocationCoordinate2D(latitude: 23.1815, longitude: 79.9864)),
                   City(name: "Vishakhapatnam", image: "Vishakhapatnam", population: "30 lakhs", coordinate: CLLocationCoordinate2D(latitude: 17.6868, longitude: 83.2185)),
                   City(name: "Solapur", image: "Solapur", population: "9.51 lakhs", coordinate: CLLocationCoordinate2D(latitude: 17.6599, longitude: 75.9064)),
                   City(name: "Davanagere", image: "Davanagere", population: "4.35 lakhs", coordinate: CLLocationCoordinate2D(latitude: 14.4644, longitude: 75.9218)),
                   City(name: "Indore", image: "Indore", population: "19.9 lakhs", coordinate: CLLocationCoordinate2D(latitude: 22.7196, longitude: 75.8577)),
                   City(name: "NMDC", image: "NMDC", population: "7 lakhs", coordinate: CLLocationCoordinate2D(latitude: 17.4550, longitude: 78.5475)),
                   City(name: "Coimbatore", image: "Coimbatore", population: "16 lakhs", coordinate: CLLocationCoordinate2D(latitude: 11.0168, longitude: 76.9558)),
                   City(name: "Kakinada", image: "Kakinada", population: "4.88 lakhs", coordinate: CLLocationCoordinate2D(latitude: 16.9891, longitude: 82.2475)),
                   City(name: "Belagavi", image: "Belagavi", population: "4.88 lakhs", coordinate: CLLocationCoordinate2D(latitude: 15.8497, longitude: 74.4977)),
                   City(name: "Udaipur", image: "Udaipur", population: "4.51 lakhs", coordinate: CLLocationCoordinate2D(latitude: 24.5854, longitude: 73.7125)),
                   City(name: "Guwahati", image: "Guwahati", population: "9.57 lakhs", coordinate: CLLocationCoordinate2D(latitude: 26.1445, longitude: 91.7362)),
                   City(name: "Chennai", image: "Chennai", population: "70.9 lakhs", coordinate: CLLocationCoordinate2D(latitude: 13.0827, longitude: 80.2707)),
                   City(name: "Ludhiana", image: "Ludhiana", population: "16.2 lakhs", coordinate: CLLocationCoordinate2D(latitude: 30.9010, longitude: 75.8573)),
                   City(name: "Bhopal", image: "Bhopal", population: "18 lakhs", coordinate: CLLocationCoordinate2D(latitude: 23.2599, longitude: 77.4126)),]
    
    init() {
        UINavigationBar.appearance().backgroundColor = .systemGroupedBackground
    }

    var body: some View {
        NavigationView {
        List {
            ForEach(Citys) { City in
                NavigationLink(destination: CityDetailView(city: City)) {
                BasicImageRow(city: City)
                    .contextMenu {
                        Button(action: {
                            self.delete(item: City)
                        }) {
                            HStack {
                                Text("Delete")
                                Image(systemName: "trash")
                            }
                        }
                        
                        Button(action: {
                            self.setFavorite(item: City)
                        }) {
                            HStack {
                                Text("Favorite")
                                Image(systemName: "star")
                            }
                        }
                    }
                }
            }
        }
            .navigationBarTitle("CITY NAMES", displayMode: .inline)
        }
    }
    
    private func delete(item City: City) {
        if let index = self.Citys.firstIndex(where: { $0.id == City.id }) {
            self.Citys.remove(at: index)
        }
    }
    
    private func setFavorite(item City: City) {
        if let index = self.Citys.firstIndex(where: { $0.id == City.id }) {
            self.Citys[index].isFavorite.toggle()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct City: Identifiable {
    var id = UUID()
    var name: String
    var image: String
    var isFavorite: Bool = false
    var population: String
    let coordinate: CLLocationCoordinate2D
}

struct BasicImageRow: View {
    var city: City
    let gradient = RadialGradient(
        gradient   : Gradient(colors: [.yellow, .red]),
        center     : UnitPoint(x: 0.25, y: 0.25),
        startRadius: 0.2,
        endRadius  : 200
    )
    // stroke line width, dash
      let w: CGFloat   = 6
      let d: [CGFloat] = [20,10]
    
    var body: some View {
            HStack {
                Image(city.image)
                    .resizable()
                    .frame(width: 60, height: 60)
                    .cornerRadius(30)
                    .clipShape(Circle())
                .overlay(
                  Circle()
                 .stroke(Color.black,lineWidth: 2)
                ).foregroundColor(Color.clear)
                
                VStack(alignment: .leading) {
                    Text(city.name).font(.headline)
                        .font(.title)
                        .foregroundColor(Color.pink)
                    Text(city.population).font(.subheadline)
                                          .font(.title)
                       }
                .padding(.leading, 4)
                
                if city.isFavorite {
                    Spacer()
                    Image(systemName: "star.fill")
                        .foregroundColor(.yellow)
                }
            }.padding(.init(top: 6, leading: 0, bottom: 6, trailing: 0))
    }
}


