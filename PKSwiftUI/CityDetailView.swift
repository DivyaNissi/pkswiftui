//
//  CityDetailView.swift
//  PKSwiftUI
//
//  Created by Divya Kothagattu on 23/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import SwiftUI
import MapKit

struct CityDetailView: View {
    var city: City
    @State private var showMap = false

    var body: some View {
        VStack {
            Image(city.image)
            .resizable()
            .aspectRatio(contentMode: .fill)
            .frame(width: 250.0, height: 250.0, alignment: .center)
            .clipShape(Circle())
            Text(city.name).font(.headline)
                .font(.title)
                .foregroundColor(Color.pink)
            Text(city.population).font(.subheadline)
                        .font(.title)
                        .foregroundColor(Color.green)
            HStack {
                    Button(action: { self.showMap = true }) {
                      Image(systemName: "mappin.and.ellipse")
                    }
                    .sheet(isPresented: $showMap) {
                        LocationMap(showModal: self.$showMap, city: City(name: self.city.name, image: self.city.image, population:self.city.population, coordinate: CLLocationCoordinate2D(latitude: self.city.coordinate.latitude, longitude: self.city.coordinate.longitude)))
                    }
            }
            Spacer()
        }.navigationBarTitle(Text(city.name),displayMode: .inline)
    }
}
struct CityDetailView_Previews: PreviewProvider {
    static var previews: some View {
        CityDetailView(city: City(name: "Bhubaneshwar", image: "Bhubaneshwar", population:"8.38 lakhs", coordinate: CLLocationCoordinate2D(latitude: 20.2961, longitude: 85.8245)))
    }
}
