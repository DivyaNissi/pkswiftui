//
//  LocationMap.swift
//  PKSwiftUI
//
//  Created by Divya Kothagattu on 24/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import SwiftUI
import MapKit

struct LocationMap: View {
  @Binding var showModal: Bool
  var city: City

  var body: some View {
    VStack {
      MapView(coordinate: city.coordinate)
      HStack {
        Text(city.name)
        Spacer()
        Button("Done") { self.showModal = false }
      }
      .padding()
    }
  }
}

struct LocationMap_Previews: PreviewProvider {
  static var previews: some View {
    LocationMap(showModal: .constant(true), city: City(name: "Bhubaneshwar", image: "Bhubaneshwar", population:"8.38 lakhs", coordinate: CLLocationCoordinate2D(latitude: 20.2961, longitude: 85.8245)))
  }
}
